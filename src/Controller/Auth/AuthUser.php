<?php

namespace App\Controller\Auth;

use App\Entity\Shared\JsonResponse;
use App\Entity\Token\Exceptions\NoTokenFoundException;
use App\Entity\Token\Exceptions\RefreshTokenException;
use App\Entity\User\Exception\UserInvalidCredentialsException;
use App\Interface\User\UserInterface;
use App\Repository\Token\TokensRepository;
use App\Service\Auth\AuthUserService;
use App\Service\User\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse as Json;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Validator\Constraints;

class AuthUser extends AbstractController
{
    #[Route('/login', methods:['POST']) ]
    public function loginCheck(UserInterface $userService,Request $request,AuthUserService $authUserService): JsonResponse
    {
        $payload = json_decode($request->getContent(),true);
        $val = new Constraints\Collection(
            [
                'email'=>new Constraints\NotBlank(),
                'pass'=> new Constraints\NotBlank()
            ]
        );
        $violations = Validation::createValidator()->validate($payload, $val);

        if ($violations->count() != 0) {
            throw new \Exception("Los parametros de la llamada no son correctos");
        }

        $email = $payload['email'];
        $pasword = $payload['pass'];

        $user = $userService->findUserByEmail($email);
        //hasheamos la contraseña
        if (hash('sha512',$pasword )==$user->getPass()){
            return new JsonResponse($authUserService->createToken($user));
        }else{
            throw new UserInvalidCredentialsException();
        }
    }
    #[Route('/loginrefresh', methods:['POST']) ]
    public function refreshToken(TokensRepository $tokensRepository,Request $request,AuthUserService $authUserService): JsonResponse
    {
        if (isset($request->headers->all()["authorization"])) {
            if ($request->getContent() == "") {
                throw new RefreshTokenException();
            }
        }
        $token = explode(" ", $request->headers->get("authorization"));
        if (count($token) != 2) {
            throw new RefreshTokenException();
        }
        $token = $token[2];
        $tokens = $tokensRepository->findUserByRefreshToken($token);

        $authUserService->updateTokens($tokens);
        return new JsonResponse($tokens);



    }
}