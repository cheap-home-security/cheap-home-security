<?php

namespace App\Controller\User;

use App\Entity\Shared\JsonResponse;
use App\Entity\User\Exception\UserMailAlreadyUsedException;
use App\Entity\User\User;
use App\Entity\User\UserId;
use App\Interface\User\UserInterface;
use App\Repository\User\UserRepository;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validation;

class PostUser extends AbstractController
{
    /**
     * @throws ORMException
     */

    #[Route('/register', methods:['POST'])]
    public function putUser(Request $request, UserInterface $userInterface): JsonResponse
    {

        $payload = json_decode($request->getContent(),true);
        $val = new Constraints\Collection(
            [
                'dni'=> new Constraints\NotBlank(),
                'name' => new Constraints\NotBlank(),
                'surname' => new Constraints\NotBlank(),
                'pass'=> new Constraints\NotBlank(),
                'email' => new Constraints\NotBlank(),
                'mobile'=> new Constraints\NotBlank(),
                'credit_card'=> new Constraints\Optional(),
                'subs'=> new Constraints\NotBlank()
            ]
        );
        $violations = Validation::createValidator()->validate($payload, $val);

        if ($violations->count() != 0) {
            throw new \Exception("Los parametros de la llamada no son correctos");
        }
        if (isset($payload['credit_card'])){
            $creditCard = $payload['credit_card'];
        }else{
            $creditCard=null;
        }
        if ($userInterface->findUserByEmail($payload['email'])){
            throw new UserMailAlreadyUsedException();
        }


        $password =  $payload['pass'];
        $password= hash('sha512', $password);
        $user = new User(
            new UserId(),
            $payload['dni'],
            $payload['name'],
            $payload['surname'],
            $password,
            $payload['email'],
            $payload['mobile'],
            "user",
            $creditCard,
            $payload['subs']
        );

        $userInterface->add($user,true);


        return new JsonResponse('Usuario registrado correctamente');
    }
}
