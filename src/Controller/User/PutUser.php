<?php

namespace App\Controller\User;

use App\Entity\Shared\JsonResponse;
use App\Entity\Shared\UuidGenerator;
use App\Entity\User\Exception\UserMailAlreadyUsedException;
use App\Entity\User\UserId;
use App\Interface\User\UserInterface;
use App\Repository\User\UserRepository;
use App\Service\Auth\AuthUserService;
use App\Service\User\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validation;

class PutUser extends AbstractController
{
    #[Route('/user/{id}', methods:['PUT'])]
    public function postUser($id,Request $request, UserInterface $userInterface): JsonResponse
    {

        $payload = json_decode($request->getContent(),true);
        $val = new Constraints\Collection(
            [
                'dni'=> new Constraints\NotBlank(),
                'name' => new Constraints\Optional(),
                'pass'=> new Constraints\Optional(),
                'email' => new Constraints\Optional(),
                'mobile'=> new Constraints\Optional(),
                'user_type'=> new Constraints\Optional(),
                'credit_card'=> new Constraints\Optional(),
                'subs'=> new Constraints\Optional()
            ]
        );
        $violations = Validation::createValidator()->validate($payload, $val);
        if ($violations->count() != 0) {
            throw new \Exception("Los parametros de la llamada no son correctos");
        }
        $user = $userInterface->findUserById(new UserId($id));

        if (isset($payload['dni'])){
            $user->setDni($payload['dni']);
        }
        if (isset($payload['name'])){
            $user->setName($payload['name']);
        }
        if (isset($payload['pass'])){
            $user->setPass($payload['pass']);
        }
        if (isset($payload['email'])){
            if ($userInterface->findUserByEmail($payload['email'] != null)){
                throw new UserMailAlreadyUsedException();
            }
            $user->setEmail($payload['email']);
        }
        if (isset($payload['mobile'])){
            $user->setMobile($payload['mobile']);
        }
        if (isset($payload['credit_card'])){
            $user->setCreditCard($payload['credit_card']);
        }
        if (isset($payload['subs'])){
            $user->setSubscripcion($payload['subs']);
        }

        $userInterface->add($user,true);


        return new JsonResponse('Usuario registrado correctamente');
    }
    #[Route('/user', methods:['PUT'])]
    public function postUserByToken($id,Request $request, UserInterface $userInterface,AuthUserService $authUserService,UserService $userService): JsonResponse
    {
        $payload = json_decode($request->getContent(),true);
        $val = new Constraints\Collection(
            [
                'dni'=> new Constraints\NotBlank(),
                'name' => new Constraints\Optional(),
                'pass'=> new Constraints\Optional(),
                'email' => new Constraints\Optional(),
                'mobile'=> new Constraints\Optional(),
                'user_type'=> new Constraints\Optional(),
                'credit_card'=> new Constraints\Optional(),
                'subs'=> new Constraints\Optional()
            ]
        );
        $violations = Validation::createValidator()->validate($payload, $val);
        if ($violations->count() != 0) {
            throw new \Exception("Los parametros de la llamada no son correctos");
        }
        $userId = $authUserService->getUserId(explode(" ",$request->headers->get('Authorization') )[0]);
        $user = $userService->findUserById(new UserId($userId));

        if (isset($payload['dni'])){
            $user->setDni($payload['dni']);
        }
        if (isset($payload['name'])){
            $user->setName($payload['name']);
        }
        if (isset($payload['pass'])){
            $user->setPass($payload['pass']);
        }
        if (isset($payload['email'])){
            if ($userInterface->findUserByEmail($payload['email'] != null)){
                throw new UserMailAlreadyUsedException();
            }
            $user->setEmail($payload['email']);
        }
        if (isset($payload['mobile'])){
            $user->setMobile($payload['mobile']);
        }
        if (isset($payload['credit_card'])){
            $user->setCreditCard($payload['credit_card']);
        }
        if (isset($payload['subs'])){
            $user->setSubscripcion($payload['subs']);
        }

        $userInterface->add($user,true);


        return new JsonResponse('Usuario registrado correctamente');
    }
}