<?php

namespace App\Controller\User;

use App\Entity\Shared\JsonResponse;
use App\Entity\User\UserId;
use App\Interface\User\UserInterface;
use App\Repository\User\UserRepository;
use App\Service\User\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DelUser extends AbstractController
{
    #[Route('/user/{id}', methods:['DELETE'])]
    public function index($id,UserService $userService,UserInterface $userRepository): JsonResponse
    {
        $userdel = $userService->findUserById(new UserId($id));
        $userRepository->remove($userdel,true);
        return new JsonResponse('Usuario Eliminado correctamente');
    }
}