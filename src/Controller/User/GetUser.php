<?php

namespace App\Controller\User;

use App\Entity\User\Exception\UserInvalidCredentialsException;
use App\Entity\User\User;
use App\Entity\User\UserId;
use App\Interface\User\UserInterface;
use App\Repository\User\UserRepository;
use App\Service\Auth\AuthUserService;
use App\Service\User\UserService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Shared\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class GetUser extends AbstractController
{
    #[Route('/user/{id}', methods:['GET']) ]
    public function index($id,UserInterface $userService): JsonResponse
    {
       $result = $userService->findUserById(new UserId($id));
       return new JsonResponse($result);
    }
    #[Route('/user', methods:['GET']) ]
    public function getUserByToken(Request $request,UserInterface $userService,AuthUserService $authUserService): JsonResponse
    {
        $userId = $authUserService->getUserId(explode(" ",$request->headers->get('Authorization') )[0]);
        $user = $userService->findUserById(new UserId($userId));
        return new JsonResponse($user);
    }





}
