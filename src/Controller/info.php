<?php
namespace App\Controller;

use App\Entity\Shared\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class info extends AbstractController
{
    #[Route('/info', methods:['GET'])]
    public function index() : JsonResponse
    {
        return new JsonResponse();
    }

}