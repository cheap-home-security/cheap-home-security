<?php

namespace App\Interface\User;

use App\Entity\User\User;
use App\Entity\User\UserId;

interface UserInterface
{
    public function add(User $entity, bool $flush = true): void;

    public function remove(User $entity, bool $flush = true): void;

    public function findUserById(UserId $id): ?User;

    public function findUserByEmail(string $email): ?User;
}