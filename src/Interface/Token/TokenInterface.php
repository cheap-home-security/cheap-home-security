<?php

namespace App\Interface\Token;

use App\Entity\Token\Tokens;
use App\Entity\User\User;

interface TokenInterface
{
    public function add(Tokens $entity, bool $flush = true): void;

    public function remove(Tokens $entity, bool $flush = true): void;

    public function findUserByRefreshToken(string $token): ?User;
}