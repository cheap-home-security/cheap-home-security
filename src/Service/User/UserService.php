<?php

namespace App\Service\User;

use App\Entity\User\Exception\UserNotFoundException;
use App\Entity\User\UserId;
use App\Repository\User\UserRepository;

class UserService
{
    public function __construct(private UserRepository $userRepository){}

    public function findUserById(UserId $userId){
        $user = $this->userRepository->findUserById($userId);
        if ($user==null) throw new UserNotFoundException();
        return $user;
}
    public function findUserByEmail($email){
        $user = $this->userRepository->findUserByEmail($email);
        if ($user==null) throw new UserNotFoundException();
        return $user;
    }

}