<?php

namespace App\Service\Auth;

use App\Entity\Token\Tokens;
use App\Entity\User\User;
use App\Interface\Token\TokenInterface;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class AuthUserService extends AbstractController
{
    public function __construct(TokenInterface $tokensRepository)
    {
        $this->tokensRepository = $tokensRepository;
    }

    public function createToken(User $user) : Tokens
    {
        //token dura 2horas
     $tiempoDeExpiracion = time() + 3600*2;
     $tokenPayload= [
         'sub' => $user->getIdUser()->value(),
         'rol' => $user->getUserType(),
         'exp' => $tiempoDeExpiracion
     ];
        //token que refresca el token anterior dura 1 semana
     $jwt = JWT::encode($tokenPayload,Tokens::TOKEN_SECRET,'HS256');
        $tiempoDeExpiracion = time() + (3600*(24*7));
        $tokenPayload= [
            'sub' => $user->getIdUser()->value(),
            'rol' => $user->getUserType(),
            'exp' => $tiempoDeExpiracion
        ];
     $jwt2= JWT::encode($tokenPayload,Tokens::REFRES_TOKEN_SECRET,'HS256');

     $token = new Tokens($jwt,$jwt2,$user,$tiempoDeExpiracion);
     $this->tokensRepository->add($token);

     return $token;
    }
    public function updateTokens(Tokens $token)
    {
        $tiempoDeExpiracion = time() + (3600*(24*7));
        $refreshTokenPayload= [
            'sub' => $token->getUser()->getIdUser()->value(),
            'rol' => $token->getUser()->getUserType(),
            'exp' => $tiempoDeExpiracion
        ];
        $tiempoDeExpiracion = time() + (3600*24);
        $tokenPayload= [
            'sub' => $token->getUser()->getIdUser()->value(),
            'rol' => $token->getUser()->getUserType(),
            'exp' => $tiempoDeExpiracion
        ];
        $jwt2= JWT::encode($refreshTokenPayload,Tokens::REFRES_TOKEN_SECRET,'HS256');
        $jwt= JWT::encode($tokenPayload,Tokens::TOKEN_SECRET,'HS256');
        $token->setToken($jwt);
        $token->setRefreshToken($jwt2);
        $token->setExpirationDate($tiempoDeExpiracion);
        $this->tokensRepository->add($token);
        return $token;
    }
    public function decodeToken($token)
    {
        $decoded = JWT::decode($token,new Key(Tokens::TOKEN_SECRET,'HS256'));
        return $decoded;
    }
    public function decodeRefreshToken($token)
    {
        $decoded = JWT::decode($token,new Key(Tokens::REFRES_TOKEN_SECRET,'HS256'));
        return $decoded;
    }
    public function getUserId($token)
    {
        $decoded = $this->decodeToken($token);
        return $decoded->sub;
    }
    public function isAdmin($token)
    {
        $decoded = $this->decodeToken($token);
        if ($decoded->rol == "Admin") {
            return true;
        }
        return false;
    }
    public function tokenExpired($token)
    {
        $decoded = $this->decodeToken($token);
        if ($decoded->exp < time()) {
            return false;
        }
        return true;
    }


}