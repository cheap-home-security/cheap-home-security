<?php

namespace App\Service\Shared;

use App\Entity\Token\Exceptions\NoTokenFoundException;
use App\Entity\Token\Exceptions\TokenExpiredException;
use App\Entity\Token\Exceptions\TokenNotValidException;
use App\Service\Auth\AuthUserService;
use Symfony\Component\HttpKernel\Event\RequestEvent;

class JwtAuthenticationListener
{
    public function __construct(AuthUserService $authUserService)
    {
        $this->authUserService = $authUserService;
    }


    public function onKernelRequest(RequestEvent $event){

    //primero ponemos un regex de las rutas en las que no queremos que se ejecute la autenticacion
    $rutasSinToken= [
        "/\/(login|register)\/?$/",
        ];
    //recojemos la uri
    $uri = $event->getRequest()->getRequestUri();
    //comprobamos si la uri esta en la lista de rutas sin token
    foreach ($rutasSinToken as $ruta){
        if(preg_match($ruta, $uri)){
            return;
        }
    }
    //si no esta en la lista, comprobamos si existe el token
    $token = $event->getRequest()->headers->get("Authorization");
    $token = explode(" ", $token);
    if(count($token) != 2){
        throw new NoTokenFoundException();
    }
    //comprobamos que el token no ha expirado y a la vez comprobamos que el token es valido
    if(!$this->authUserService->tokenExpired($token[0])){
        throw new TokenExpiredException();
    }
    //miramos si el token es de un admin o un usuario
    if (!$this->authUserService->isAdmin($token[0])){
        //si es de un usuario comprobamos que el esta accediendo a una ruta valida de usuario
        $rutasDeUsuario=[
            "/\/(loginrefresh|user|info)\/?$/",
            "/\/user\/([0-9-a-z]{36}\/?)$/"
        ];
        //recorremos el array de rutas de usuario
        foreach ($rutasDeUsuario as $ruta){
            if(preg_match($ruta, $uri)){
                //si esta dentro de la lista de rutas de usuario esta bien y sale
                return;
            }
        }
        //si no es una ruta que no es valida para un usuario, lanzamos una excepcion
        throw new TokenNotValidException();
    }else{
        //si es un admin no hay que hacer comprobaciones
        return;
    }



    }
}