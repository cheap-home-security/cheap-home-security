<?php
namespace App\Service\Shared;

use Doctrine\DBAL\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class ErrorController
{
    public function render(Request $request, Exception $exception): JsonResponse {
        return new JsonResponse($exception->getMessage(), $exception->getCode());
    }
    public function onKernelException(ExceptionEvent $event)
    {
        // You get the exception object from the received event
        $exception = $event->getThrowable();
        // Get incoming request
        $request   = $event->getRequest();
         if ($exception->getCode()==0){
                $code = 500;

            }else{
                $code = $exception->getCode();
            }
            // Customize your response object to display the exception details
            $date = new \DateTime();
            $response = new JsonResponse([
                'code'          => $code,
                'time'          => $date->getTimestamp(),
                'message'       => $exception->getMessage(),
            ]);
            // HttpExceptionInterface is a special type of exception that
            // holds status code and header details
            if ($exception instanceof HttpExceptionInterface) {
                $response->headers->replace($exception->getHeaders());
                $response->setStatusCode($exception->getStatusCode());

            } else {
                $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
            }

            // sends the modified response object to the event
            $event->setResponse($response);
        }


//    }
}

