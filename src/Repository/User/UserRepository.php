<?php

namespace App\Repository\User;

use App\Entity\User\User;
use App\Entity\User\UserId;
use App\Interface\User\UserInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(User $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function findUserById(UserId $id): ?User{

        $query= $this->getEntityManager()
            ->getRepository(User::class)
            ->createQueryBuilder("a")
            ->where("a.idUser = :id")
            ->setParameter("id",$id->value())
            ->getQuery();
//        dd($query);
        $result = $query->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT);
        return $result;
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(User $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
    public function findUserByEmail($email): ?User
    {
        $query= $this->getEntityManager()
            ->getRepository(User::class)
            ->createQueryBuilder("a")
            ->where("a.Email = :id")
            ->setParameter("id",$email)
            ->getQuery();
        $result = $query->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT);
        return $result;

    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
