<?php

namespace App\Repository\Token;

use App\Entity\Token\Tokens;
use App\Entity\User\User;
use App\Interface\Token\TokenInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;
use Pci\Domain\Token\TokenPair;

/**
 * @method Tokens|null find($id, $lockMode = null, $lockVersion = null)
 * @method Tokens|null findOneBy(array $criteria, array $orderBy = null)
 * @method Tokens[]    findAll()
 * @method Tokens[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TokensRepository extends ServiceEntityRepository implements TokenInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tokens::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Tokens $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Tokens $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }
    public function findUserByRefreshToken(string $token): ?User
    {
       $query = $this->entityManager()
           ->createQueryBuilder()
           ->select('tp')
           ->from(Tokens::class, 't')
           ->where('t.refreshToken = ?1')
           ->setParameter(1, $token);
       return $query->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_OBJECT);
    }


    // /**
    //  * @return Tokens[] Returns an array of Tokens objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Tokens
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
