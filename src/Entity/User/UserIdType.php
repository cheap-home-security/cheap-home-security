<?php

namespace App\Entity\User;


use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;



class UserIdType extends Type
{
    const USER_ID = 'linea_presupuesto_id';

    public function getName() {
        return static::USER_ID;
    }
    public function getSQLDeclaration(array $column,  $platform) {
        return 'VARCHAR(50)';
    }
    public function convertToPHPValue($value, AbstractPlatform $platform) {
        return new UserId($value);
    }
    protected function getValueObjectClassName(): string {
        return UserId::class;
    }
}