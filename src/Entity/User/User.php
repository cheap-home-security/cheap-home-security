<?php

namespace App\Entity\User;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="App\Repository\User\UserRepository")
 */
class User extends AbstractController implements \JsonSerializable
{
    /**
     * @var UserId
     *
     * @ORM\Column(name="id_user", type="user_id", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private UserId $idUser;

    /**
     * @var string
     *
     * @ORM\Column(name="dni", type="string", length=9, nullable=false)
     */
    private string $Dni;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private string $Name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=false)
     */
    private string $Surname;

    /**
     * @var string
     *
     * @ORM\Column(name="pass", type="string", length=255, nullable=false)
     */
    private string $Pass;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string",unique=true, length=255, nullable=false)
     */
    private string $Email;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", nullable=false)
     */
    private string $Mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="user_type", type="string", length=5, nullable=false)
     */
    private string $UserType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="credit_card", type="string", length=19, nullable=true, options={"default"="NULL"})
     */
    private ?string $CreditCard = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="subscripcion", type="string", length=4, nullable=false)
     */
    private string $subscripcion;

    /**
     * User constructor.
     * @param UserId $idUser
     * @param string $Dni
     * @param string $Name
     * @param string $Surname
     * @param string $Pass
     * @param string $Email
     * @param string $Mobile
     * @param string $UserType
     * @param string|null $CreditCard
     * @param string $subscripcion
     */
    public function __construct(UserId $idUser, string $Dni, string $Name, string $Surname, string $Pass, string $Email, string $Mobile, string $UserType, ?string $CreditCard, string $subscripcion)
    {
        $this->idUser = $idUser;
        $this->Dni = $Dni;
        $this->Name = $Name;
        $this->Surname = $Surname;
        $this->Pass = $Pass;
        $this->Email = $Email;
        $this->Mobile = $Mobile;
        $this->UserType = $UserType;
        $this->CreditCard = $CreditCard;
        $this->subscripcion = $subscripcion;
    }


    /**
     * @return string
     */
    public function getIdUser(): UserId
    {
        return $this->idUser;
    }

    /**
     * @param string $idUser
     */
    public function setIdUser(UserId $idUser): void
    {
        $this->idUser = $idUser;
    }

    /**
     * @return string
     */
    public function getDni(): string
    {
        return $this->Dni;
    }

    /**
     * @param string $Dni
     */
    public function setDni(string $Dni): void
    {
        $this->Dni = $Dni;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->Name;
    }

    /**
     * @param string $Name
     */
    public function setName(string $Name): void
    {
        $this->Name = $Name;
    }

    /**
     * @return string
     */
    public function getPass(): string
    {
        return $this->Pass;
    }

    /**
     * @param string $Pass
     */
    public function setPass(string $Pass): void
    {
        $this->Pass = $Pass;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->Email;
    }

    /**
     * @param string $Email
     */
    public function setEmail(string $Email): void
    {
        $this->Email = $Email;
    }

    /**
     * @return int
     */
    public function getMobile(): int|string
    {
        return $this->Mobile;
    }

    /**
     * @param int $Mobile
     */
    public function setMobile(int|string $Mobile): void
    {
        $this->Mobile = $Mobile;
    }

    /**
     * @return string
     */
    public function getUserType(): string
    {
        return $this->UserType;
    }

    /**
     * @param string $UserType
     */
    public function setUserType(string $UserType): void
    {
        $this->UserType = $UserType;
    }

    /**
     * @return string|null
     */
    public function getCreditCard(): ?string
    {
        return $this->CreditCard;
    }

    /**
     * @param string|null $CreditCard
     */
    public function setCreditCard(?string $CreditCard): void
    {
        $this->CreditCard = $CreditCard;
    }

    /**
     * @return string
     */
    public function getSubscripcion(): string
    {
        return $this->subscripcion;
    }

    /**
     * @param string $subscripcion
     */
    public function setSubscripcion(string $subscripcion): void
    {
        $this->subscripcion = $subscripcion;
    }

    public function jsonSerialize()
    {
       $a=[
           "id_user"=>$this->idUser->value(),
           "dni"=>$this->Dni,
           "name"=>$this->Name,
           "surname"=>$this->Surname,
           "pass"=>$this->Pass,
           "email"=>$this->Email,
           "mobile"=>$this->Mobile,
           "user_type"=>$this->UserType,
           "credit_card"=>$this->CreditCard,
           "subs"=>$this->subscripcion,

       ];

    return $a;
    }
}
