<?php

namespace App\Entity\User\Exception;


class UserMailAlreadyUsedException extends  \Exception
{
    public function __construct() {
        parent::__construct("El mail ya esta en uso", 409);
    }
}