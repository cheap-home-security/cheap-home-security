<?php

namespace App\Entity\User\Exception;


class UserInvalidCredentialsException extends  \Exception
{
    public function __construct() {
        parent::__construct("Las crendenciales no son correctas");
    }
}