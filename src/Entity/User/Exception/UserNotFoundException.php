<?php

namespace App\Entity\User\Exception;




class UserNotFoundException extends  \Exception
{
        public function __construct() {
        parent::__construct("Usuario no encontrado");
    }
}