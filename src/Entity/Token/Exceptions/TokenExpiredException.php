<?php

namespace App\Entity\Token\Exceptions;

class TokenExpiredException extends \Exception
{
    public function __construct() {
        parent::__construct("El token no es válido",403);
    }

}