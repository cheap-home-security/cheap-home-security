<?php

namespace App\Entity\Token\Exceptions;

use JetBrains\PhpStorm\Internal\LanguageLevelTypeAware;

class RefreshTokenException extends \Exception
{
    public function __construct() {
        parent::__construct("Ha habido un error con el refresh token",405);
    }
}