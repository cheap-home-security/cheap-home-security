<?php

namespace App\Entity\Token\Exceptions;

class NoTokenFoundException extends \Exception
{
    public function __construct() {
        parent::__construct("No se ha encontrado el token",401);
    }

}