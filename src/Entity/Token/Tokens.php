<?php

namespace App\Entity\Token;

use App\Entity\User\User;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Tokens
 *
 * @ORM\Table (name="tokens")
 * @ORM\Entity(repositoryClass="App\Repository\Token\TokensRepository")
 */

class Tokens extends AbstractController implements \JsonSerializable
{
    public const TOKEN_SECRET = 'ThisTokenIsNotSoSecretChangeIt';
    public const REFRES_TOKEN_SECRET = 'ThisRefreshTokenIsNotSoSecretChangeIt';
    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=255)
     */
    private $token;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $refreshToken;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id_user")
     */
    private $user;

    /**
     * @ORM\Column(type="integer")
     */
    private $expirationDate;

    public function __construct(
        string $token,
        string $refreshToken,
        User $user,
        int $expirationDate
    ) {
        $this->token = $token;
        $this->refreshToken = $refreshToken;
        $this->user = $user;
        $this->expirationDate = $expirationDate;
    }



    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    public function setRefreshToken(string $refreshToken): self
    {
        $this->refreshToken = $refreshToken;

        return $this;
    }

    public function getUserObject() : User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getExpirationDate(): ?int
    {
        return $this->expirationDate;
    }

    public function setExpirationDate(int $expirationDate): self
    {
        $this->expirationDate = $expirationDate;

        return $this;
    }
    public function jsonSerialize()
    {
        $a=[
            "token"=>$this->token,
            "refreshToken"=>$this->refreshToken
        ];
        return $a;
    }
}
