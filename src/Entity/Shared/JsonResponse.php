<?php

namespace App\Entity\Shared;

class JsonResponse extends \Symfony\Component\HttpFoundation\JsonResponse
{
    public function __construct($data = null, int $status = 200, array $headers = [], bool $json = false, $message = null)
    {
        $headers['Access-Control-Allow-Origin'] = "*";
        $headers["Access-Control-Allow-Headers"] = "Origin, Content-Type, Accept, Access-Control-Request-Method, Authorization";
        $headers["Access-Control-Allow-Methods"] = "POST, GET, OPTIONS, DELETE, PUT";
        $base = [
            "code" => $status,
            "timestamp" => time()
        ];
        if ($status == 200) {
            if (!is_null($data)) {
                $base['data'] = $data;
            }
            if (!is_null($message)) {
                $base['message'] = $message;
            }
        } else {
            $base['error'] = $data;
        }
        parent::__construct($base, $status, $headers, $json);
        $this->setEncodingOptions($this->getEncodingOptions() | JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT | JSON_HEX_QUOT | JSON_HEX_APOS);
    }

}