<?php

namespace App\Entity\Shared;

use App\Entity\Shared\Exceptions\UuidNotValidException;
use Ramsey\Uuid\Nonstandard\Uuid;


class UuidGenerator
{


    public function __construct(protected string $value = '') {
        if ($this->value == '') {
            $this->value = self::generate();
        }
        $this->ensureIsValidUuid($this->value);
    }
    public static function generate(): string {
        return Uuid::uuid4()->toString();
    }

    private function ensureIsValidUuid(string $value){
//        dd($value);
        if (!Uuid::isValid($value)) throw new UuidNotValidException;
    }


    public static function new(): self {
        return new static(self::generate());
    }

    public function equals(HashedValueObject $otherValue): bool {
        return $this->value() === $otherValue->value();
    }

    public function value(): string {
        return $this->value;
    }
    public function __toString(): string {
        return $this->value();
    }


}