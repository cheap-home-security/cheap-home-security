<?php

namespace App\Entity\Shared\Exceptions;

class UuidNotValidException extends \Exception
{
    public function __construct() {
        parent::__construct("Uuid no valido");
    }


}